#include "do.h"

#include <xdo.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

static xdo_t *xdo = NULL;
static int fds[2] = {0};
pid_t childpid = 0;

static int retried = 0;
static struct do_msg prev_msg;

static void do_child_proc() {
	close(fds[1]);

	// Restore signals
	struct sigaction sa;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = SIG_DFL;
	sigaction(SIGCHLD, &sa, NULL);
	sigaction(SIGPIPE, &sa, NULL);
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);

	xdo = xdo_new(getenv("DISPLAY"));
	if (xdo == NULL) {
		printf("Creating xdo instance failed.\n");
		exit(EXIT_FAILURE);
	}

	struct do_msg msg;
	while (1) {
		ssize_t len = read(fds[0], &msg, sizeof(msg));
		if (len < 0) {
			perror("child: read:");
			exit(EXIT_FAILURE);
		} else if (len < (ssize_t)sizeof(msg)) {
			printf("child: short read (%zi < %zi). Exiting.\n",
				len, sizeof(msg));
			exit(EXIT_FAILURE);
		}

		switch (msg.type) {
			case DO_KEY:
				xdo_send_keysequence_window(
					xdo, CURRENTWINDOW, msg.m.key, 12000);
				break;
			case DO_TEXT:
				xdo_enter_text_window(
					xdo, CURRENTWINDOW, msg.m.text, 12000);
				break;
			case DO_CLICK:
				xdo_click_window(
					xdo, CURRENTWINDOW, msg.m.click);
				break;
			case DO_MOVE:
				xdo_move_mouse_relative(
					xdo, msg.m.move.x, msg.m.move.y);
				break;
			case DO_EXIT:
			exit(EXIT_SUCCESS);
			break;
		}
	}
}

static void do_fork() {
	if (pipe(fds) < 0) {
		perror("do_fork: pipe");
		exit(EXIT_FAILURE);
	}

	childpid = fork();
	if (childpid == -1) {
		perror("do_fork: fork");
		exit(EXIT_FAILURE);
	}

	// Parent process
	if (childpid != 0) {
		close(fds[0]);

	// Child process
	} else {
		do_child_proc();
	}
}

void onsigchld(int sig, siginfo_t *info, void *ctx) {
	(void)sig;
	(void)ctx;

	if (!retried && info->si_status == CLD_EXITED) {
		do_fork();
		do_send(&prev_msg);
		retried = 1;
	}
}

void do_init() {
	struct sigaction sa;

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = SIG_IGN;
	sigaction(SIGPIPE, &sa, NULL);
	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = onsigchld;
	sigaction(SIGCHLD, &sa, NULL);

	do_fork();
}

void do_free() {
	struct do_msg msg = { DO_EXIT, {} };
	if (write(fds[1], &msg, sizeof(msg)) < 0) {
		if (errno == EPIPE) {
			perror("do_free: write");
			exit(EXIT_FAILURE);
		}
	}
}

void do_send(struct do_msg *msg) {
	retried = 0;
	prev_msg = *msg;
	ssize_t n = write(fds[1], msg, sizeof(*msg));
	if (n < 0) {
		if (errno == EPIPE) {
			printf("\nPipe is broken, spawning new child.\n");
			do_fork();
			if (write(fds[1], msg, sizeof(*msg)) < 0)
				perror("do_send: write");
		} else {
			perror("do_send: write");
		}
	}
}

void do_key(char *key) {
	struct do_msg msg = { DO_KEY, .m.key = {0} };
	strncpy(msg.m.key, key, sizeof(msg.m.key));
	do_send(&msg);
}
void do_text(char *text) {
	struct do_msg msg = { DO_TEXT, .m.text = {0} };
	strncpy(msg.m.text, text, sizeof(msg.m.text));
	do_send(&msg);
}
void do_click(int btn) {
	struct do_msg msg = { DO_CLICK, .m.click = btn };
	do_send(&msg);
}
void do_move(int x, int y) {
	struct do_msg msg = { DO_MOVE, .m.move = { x, y } };
	do_send(&msg);
}
