#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <signal.h>

#include "keys.h"
#include "do.h"

#ifndef REMOTEDOTOOL_VERSION
#define REMOTEDOTOOL_VERSION "unknown"
#endif

#define arrlength(x) (sizeof(x) / sizeof(*x))
#define streq(a, b) (strcmp((a), (b)) == 0)

static int debug = 0;
static int mouse_mode = 0;

static void cleanup() {
	do_free();
}

static void onterm(int signum) {
	(void)signum;
	cleanup();
	exit(EXIT_FAILURE);
}

static void raw_on() {
	system("stty raw opost -echo");
}
static void raw_off() {
	system("stty sane");
}

static int run_shellcmd(char *shellcmd) {
	if (streq(shellcmd, "exit") || streq(shellcmd, "x")) {
		cleanup();
		exit(EXIT_SUCCESS);
	} else if (streq(shellcmd, "debug")) {
		debug = 1;
		printf("Debug mode enabled. Run 'nodebug' to disable.\n");
	} else if (streq(shellcmd, "nodebug")) {
		debug = 0;
		printf("Debug mode disabled.\n");
	} else if (streq(shellcmd, "mouse")) {
		mouse_mode = 1;
		printf("Mouse mode enabled. Run 'nomouse' to disable.\n");
		printf("  Move mouse: <arrow key>, Ctrl+<arrow key>, Shift+<arrow key>\n");
		printf("  Left click: Ctrl+z, Middle click: Ctrl+x, Right click: Ctrl+c\n");
		printf("  Scroll up: Ctrl+q, Scroll down: Ctrl+a\n");
	} else if (streq(shellcmd, "nomouse")) {
		mouse_mode = 0;
		printf("Mouse mode disabled.\n");
	} else if (strstr(shellcmd, "key ") == shellcmd) {
		char *key = strstr(shellcmd, " ") + 1;
		printf("%s\n", key);
		do_key(key);
	} else {
		return -1;
	}

	return 0;
}

static void onkey(char *name, int matched) {
	if (debug)
		printf("%s\n", name);
	else
		printf("%s ", name);
	fflush(stdout);

	// Shell
	if (streq(name, "Ctrl+y")) {
		raw_off();
		char *shellcmd = readline(
			"\nShell. Type 'x' or 'exit' to exit, 'debug' to enter debug mode, "
			"'mouse' to enter mouse mode, 'key <key>' to enter a key manually.\n> ");
		if (shellcmd == NULL) {
			printf("\n");
		} else {
			add_history(shellcmd);

			if (run_shellcmd(shellcmd) < 0)
				system(shellcmd);

			free(shellcmd);
		}
		raw_on();

	// Don't do anything else in debug mode
	} else if (debug) {
		// Nothing

	// Move mouse
	} else if (mouse_mode && streq(name, "Left")) {
		do_move(-100, 0);
	} else if (mouse_mode && streq(name, "Right")) {
		do_move(100, 0);
	} else if (mouse_mode && streq(name, "Up")) {
		do_move(0, -100);
	} else if (mouse_mode && streq(name, "Down")) {
		do_move(0, 100);

	// Move mouse slower
	} else if (mouse_mode && streq(name, "Ctrl+Left")) {
		do_move(-20, 0);
	} else if (mouse_mode && streq(name, "Ctrl+Right")) {
		do_move(20, 0);
	} else if (mouse_mode && streq(name, "Ctrl+Up")) {
		do_move(0, -20);
	} else if (mouse_mode && streq(name, "Ctrl+Down")) {
		do_move(0, 20);

	// Move mouse slowest
	} else if (mouse_mode && streq(name, "Shift+Left")) {
		do_move(-5, 0);
	} else if (mouse_mode && streq(name, "Shift+Right")) {
		do_move(5, 0);
	} else if (mouse_mode && streq(name, "Shift+Up")) {
		do_move(0, -5);
	} else if (mouse_mode && streq(name, "Shift+Down")) {
		do_move(0, 5);

	// Click
	} else if (mouse_mode && streq(name, "Ctrl+z")) {
		do_click(1);
	} else if (mouse_mode && streq(name, "Ctrl+x")) {
		do_click(2);
	} else if (mouse_mode && streq(name, "Ctrl+c")) {
		do_click(3);
	} else if (mouse_mode && streq(name, "Ctrl+q")) {
		do_click(4);
	} else if (mouse_mode && streq(name, "Ctrl+a")) {
		do_click(5);

	// Press key
	} else {
		if (matched) {
			do_key(name);
		} else {
			do_text(name);
		}
	}
}

static uint32_t read_utf8(unsigned char first, int fd) {
	int bytesleft;
	uint8_t ch = first;
	if (first >= 0b11110000) {
		bytesleft = 3;
		ch &= 0b00001111;
	} else if (first >= 0b11100000) {
		bytesleft = 2;
		ch &= 0b00011111;
	} else if (first >= 0b11000000) {
		bytesleft = 1;
		ch &= 0b00111111;
	} else {
		return ch;
	}

	uint32_t codepoint = ch << (bytesleft * 6);
	while (bytesleft > 0) {
		bytesleft -= 1;
		read(fd, &ch, 1);
		ch &= 0b00111111;
		codepoint |= ch << (bytesleft * 6);
	}

	return codepoint;
}

static void usage(char *argv0) {
	printf(
		"Usage: %1$s              - Start remotedotool\n"
		"Usage: %1$s <comand...>  - Run commands before starting interactively\n"
		"       %1$s -h,--help    - Show this help text and exit\n"
		"       %1$s -v,--version - Print version and exit\n",
		argv0);
}

int main(int argc, char **argv) {
	if (argc == 2) {
		if (streq(argv[1], "-v") || streq(argv[1], "--version")) {
			printf("Remotedotool %s\n", REMOTEDOTOOL_VERSION);
			return EXIT_SUCCESS;
		} else if (streq(argv[1], "-h") || streq(argv[1], "--help")) {
			usage(argv[0]);
			return EXIT_SUCCESS;
		}
	}

	char *display = getenv("DISPLAY");
	if (display == NULL) {
		printf("DISPLAY variable empty, assuming ':0'.\n");
		setenv("DISPLAY", ":0", 0);
	}

	do_init();

	// Attach exit handler
	struct sigaction sa;
	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = onterm;
	sigaction(SIGTERM, &sa, NULL);
	sigaction(SIGINT, &sa, NULL);

	// Run through argc
	for (int i = 1; i < argc; ++i) {
		if (run_shellcmd(argv[i]) < 0)
			printf("Unknown command: '%s'\n", argv[i]);
	}

	raw_on();
	printf("Press Ctrl+y for a shell.\n");

	int keycount = arrlength(keys);

	int inputidx = 0;
	struct key matches[arrlength(keys)];
	int matchlen = 0;
	int matchcount = 0;
	int matchidx = 0;

	while (1) {
		unsigned char c = 0;
		int none = 0;

		// Read with a timeout
		fd_set set;
		FD_ZERO(&set);
		FD_SET(STDIN_FILENO, &set);
		struct timeval timeout = { .tv_sec = 0, .tv_usec = 10000 };
		int numfd = select(STDIN_FILENO + 1, &set, NULL, NULL, &timeout);
		if (numfd < 0) {
			if (errno == EINTR)
				continue;
			perror("stdin");
			return EXIT_FAILURE;
		} else if (numfd == 0) {
			none = 1;
		} else {
			read(STDIN_FILENO, &c, 1);
		}

		// Print char in debug mode
		if (!none && debug) {
			if (c >= 0x20 && c <= 0x7e)
				printf("\r%.2x (%c)\n", c, c);
			else
				printf("\r%.2x (unprintable)\n", c);
		}

		char utf8_name[8];
		utf8_name[0] = '\0';

		// Parse UTF-8 character if the char starts with 1
		if (!none && c >= 0x80) {
			uint32_t ch = read_utf8(c, STDIN_FILENO);
			snprintf(utf8_name, sizeof(utf8_name), "U%x", ch);
		}

		// If the read timed out with nothing buffered,
		// just do nothing
		if (none && inputidx == 0) {
			matchcount = -1;

		// If the read timed out and we have something buffered,
		// find the thing which matches (probably escape)
		} else if (none) {
			matchcount = -1;
			for (int i = 0; i < matchlen; ++i) {
				if (matches[i].name != NULL && matches[i].codelen == inputidx) {
					matchidx = i;
					matchcount = 1;
					break;
				}
			}

		// Init matches if it's the first character
		} else if (inputidx == 0) {
			for (int i = 0; i < keycount; ++i) {
				if (keys[i].code[inputidx] == c) {
					matchcount += 1;
					memcpy(&matches[matchlen], &keys[i], sizeof(*keys));
					matchidx = matchlen;
					matchlen += 1;
				}
			}

		// Continue matching if it's not the first character
		} else {
			for (int i = 0; i < matchlen; ++i) {
				if (matches[i].name == NULL)
					continue;
				if (matches[i].codelen < inputidx || matches[i].code[inputidx] != c) {
					matchcount -= 1;
					matches[i].name = NULL;
				} else {
					matchidx = i;
				}
			}
		}

		inputidx += 1;

		// Continue matching if the one match isn't done
		if (!none && matchcount == 1 && inputidx != matches[matchidx].codelen)
			continue;

		// Invoke xdotool if appropriate
		if (matchcount == 1) {
			onkey(matches[matchidx].name, 1);
		} else if (matchcount == 0 && utf8_name[0]) {
			onkey(utf8_name, 1);
		} else if (matchcount == 0) {
			char name[] = { c, '\0' };
			onkey(name, 0);
		} else if (matchcount == -1 && !none) {
			printf(" Warning: unknown key sequence\n");
		}

		if (matchcount == 1 || matchcount == 0 || matchcount == -1) {
			inputidx = 0;
			matchlen = 0;
			matchcount = 0;
		}
	}

	raw_off();
	cleanup();
	return EXIT_SUCCESS;
}
