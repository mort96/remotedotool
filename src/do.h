#ifndef DO_H
#define DO_H

enum do_type {
	DO_KEY,
	DO_TEXT,
	DO_CLICK,
	DO_MOVE,
	DO_EXIT,
};

struct do_msg {
	enum do_type type;
	union {
		char key[128];
		char text[128];
		int click;
		struct { int x; int y; } move;
	} m;
};

void do_init();
void do_free();

void do_send(struct do_msg *msg);

void do_key(char *key);
void do_text(char *text);
void do_click(int btn);
void do_move(int x, int y);

#endif
