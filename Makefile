PREFIX ?= /usr/local
FLAGS = -Wall -Wextra -g -lreadline -Ixdotool -lX11 -lXinerama -lxkbcommon -lXtst $(CFLAGS) $(LDFLAGS)
VERSION = $(shell git describe --tags --dirty)
SRC = src/main.c src/do.c
HDR = src/do.h src/keys.h

remotedotool: xdotool/libxdo.a $(SRC) $(HDR)
	$(CC) $(FLAGS) -DREMOTEDOTOOL_VERSION=\"$(VERSION)\" -o $@  $(SRC) $(HDR) $<

xdotool/libxdo.a: xdotool/.git
	make -C xdotool libxdo.a

xdotool/.git:
	git submodule update --init xdotool

.PHONY: install
install: remotedotool
	cp remotedotool $(PREFIX)/bin

.PHONY: clean
clean:
	rm -f remotedotool
	make -C xdotool clean
