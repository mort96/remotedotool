# Remotedotool

This tool is for sending keyboard and mouse input interactively. For example,
if you have some system with a graphical display but without a keyboard (for
example an embedded device with a screen, some kind of media PC, or just your
other computer across the room), remotedotool can be used to control the
computer graphically from SSH.

![Video](https://giant.gfycat.com/InsistentOddBronco.webm)

## Compile/install

	make && sudo make install

The `remotedotool` binary will be installed to `/usr/local/bin/remotedotool` by
default, but the makefile respects `PREFIX`.

Cross compilation is done by setting CC and CFLAGS appropriately (for example
`make CC=aarch64-linux-gnu-gcc CFLAGS='--sysroot=/path/to/my/target/sysroot'`).

## Usage

Run:

	remotedotool

Run in debug mode (meaning no keys will be pressed):

	remotedotool debug

For the most part, remotedotool tries to emulate the keys you pressed on your
keyboard, with these exceptions:

* Ctrl+Y: Open a prompt.
	* `exit`: Exit the program.
	* `debug`, `nodebug`: Enter/exit debug mode.
	* `mouse`, `nomouse`: Enter/exit mouse mode.
	* `key <keystroke>`: Send a key sequence (like `xdotool key <keystroke>`).
	* Anything else: Executed by your `$SHELL`.
* In mouse mode:
	* Arrow keys move the mouse cursor.
	* Ctrl+arrow keys move slower, Shift+arrow keys move slowest.
	* Ctrl+z, Ctrl+x, and Ctrl+c click with the left, middle, and right
	  mouse button.
	* Ctrl+q and Ctrl+a scrolls up and down.
* Some keys are sent as the same byte sequence as other keys:
	* Ctrl+h is Ctrl+BackSpace.
	* Ctrl+i is Tab.
	* Ctrl+m is Enter.
